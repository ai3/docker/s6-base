FROM debian:bullseye-slim

ENV S6_READ_ONLY_ROOT=1 \
    S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
    S6_KEEP_ENV=1 \
    S6_LOGGING=0

COPY etc/ /etc/
COPY deb_autistici_org.gpg /usr/share/keyrings/deb.autistici.org.gpg
COPY every /usr/bin/every

ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.3/s6-overlay-amd64.tar.gz /tmp/

# The RUN directive does the following:
#
# * Install our own Debian package repository (so it is available
#   for downstream images, we don't use it directly here).
#
# * Decompress and install the s6-overlay distribution.
#
# * Cheap trick to get s6-overlay to work with read-only root and
#   a /run directory with noexec: just replace s6-hiercopy with the
#   "cp -sR" command, which will create a copy of the source dir but
#   where all files are symlinks. This way, s6 can execute the
#   scripts off the original dir, and write the supervision sockets
#   in the service directories (/var/run/s6).
#
# * To work around another similar issue, we also disable the
#   init-stage2.fixattrs stage, which won't work due to the read-only fs.
#
RUN tar -C / -xzf /tmp/s6-overlay-amd64.tar.gz && \
    chmod 1777 /run && \
    echo "deb [signed-by=/usr/share/keyrings/deb.autistici.org.gpg] http://deb.autistici.org/urepo float/bullseye/" > /etc/apt/sources.list.d/float.list && \
    sed -i -e 's/s6-hiercopy/cp -sR/g' \
	/etc/s6/init/init-stage2 \
	/etc/s6/init-catchall/init-stage1 \
	/etc/s6/init-no-catchall/init-stage1 && \
    rm -f /usr/bin/fix-attrs && \
    ln -s /bin/true /usr/bin/fix-attrs

ENTRYPOINT ["/init"]
