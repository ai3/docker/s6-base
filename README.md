s6-base
===

This is a base container image for multi-daemon containers that
leverages [s6-overlay](https://github.com/just-containers/s6-overlay/)
as an internal minimal *init* system, on top of a standard Debian
"slim" image.

It is meant as an almost drop-in replacement for
[chaperone-base](https://git.autistici.org/ai3/docker/chaperone-base),
now that Chaperone is no longer under active development. It is also a
bit lighter, thanks to not requiring Python 3, resulting in one case
in a 40MB saving. S6 is also a lot lighter at runtime.

## Usage

### Initialization scripts

It is possible to run scripts at container startup time, before any
other services are started, by dropping files in the /etc/cont-init.d/
directory. Files should be executable. As a convention, we're using
run-parts-like naming, *NNscriptname* (NN is a number).

### Primary services

Primary services cause the whole container to exit when they fail.

Create a directory named after the service below /etc/services.d/,
which should contain:

* a *run* script, which will execute the service
* a *finish* script, with the following contents:

```
#!/usr/bin/execlineb -S0
s6-svscanctl -t /var/run/s6/services
```

Both files should be executable.

### Secondary services

Secondary services will just be silently restarted by s6 on failure,
without affecting the rest of the services in the container.

Create a directory named after the service below /etc/services.d/, and
put a *run* script in there which will execute the service. Make sure
the script is executable.

### Cron jobs

S6-overlay has no built-in facility for cron jobs, so while one
alternative would be to install Debian's *cron* package, in most cases
it is simpler to create a secondary service that will run a loop with
*sleep*:

```
#!/bin/sh

period=600

while true; do
    sleep $period
    /usr/bin/my-command foo bar
done
```

To simplify this problem, we ship a simple shell-based "cron job
runner" that implements a sligthly more sophisticated version of the
above loop (with random initial delay to stagger jobs on restart)
called *every*. The above example should then be written like this:

```
#!/bin/sh
exec every 600 /usr/bin/my-command foo bar
```

## Differences with stock *s6-overlay*

The base s6-overlay distribution is modified slightly to work around
the particularly strict environment found in
[float](https://git.autistici.org/ai3/float), which does not offer any
directory which is both writeable, and where scripts can be executed
(all tmpfs mounts have the *noexec* bit set). This does not play well
with s6, which need to create stuff in the service directory, which
also contains the *run* script.

Our simple solution is to have s6-overlay copy all its configuration
to /var/run/s6 (on the /run tmpfs), so that it is writeable, and then
to make the *run* scripts symlinks back to /etc, so that they can be
executed.

We achieve the former by setting [S6_READ_ONLY_ROOT=1 environment
variable](https://github.com/just-containers/s6-overlay#read-only-root-filesystem)
in the container, and the latter by replacing the usage of
[s6-hiercopy](https://skarnet.org/software/s6-portable-utils/s6-hiercopy.html)
with "cp -sR", which creates the target directory structure but then
uses symlinks instead of copying files.
